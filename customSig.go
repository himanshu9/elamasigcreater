package main

import (
	// "strconv"
	"fmt"
	"crypto/ecdsa"

	// "bufio"
	"crypto/elliptic"
	// "crypto/md5"
	"crypto/sha256"
	// "crypto/x509"
	"crypto/rand"
  //   "encoding/json"

	// "hash"
	"encoding/hex"
	// "encoding/pem"
	"encoding/base64"
	// "io"
	// "math/big"
	 "os"
)



func CreateMintAccount() (tx string){	

	_,priPEMData := readPemFromDisk("mintPrivateKey")
	
	
	private,_:= parse_pem_with_hex_private(priPEMData);

	var pubkey ecdsa.PublicKey
	pubkey = private.PublicKey

	
	
	//end creating key pairs
 

  
	hexKey := elliptic.Marshal(S256(), pubkey.X, pubkey.Y)
	
	hexFormat :=Encode(hexKey)[4:]
	//convert pem public key to der format
	 pub, _ :=  parse_hex_public([]byte(hexFormat))
	 
  
	
   //convert der format public key to address
	 addr := make_pubkey_to_addr(pub)
 
	 formattedAddr := fmt.Sprintf("%x", addr) 
	 

// 	//end der to address conversion
  
//    //creating transaction struct
	transaction := PrototypeTransaction{}
	transaction.Pubkey = formattedAddr
	
//    //end creating transaction Struct
  
  
	//serialize transaction
	serializedTx,_ := transaction.Serialize()

	//encode serialize transaction in to base64
	rawContext := base64.StdEncoding.EncodeToString(serializedTx)
	//end encoding
  
	//create hash of encoded transaction
   
	 h := sha256.New()
	h.Write([]byte(rawContext))
	signHash := h.Sum(nil)
	// fmt.Printf("%x", h.Sum(nil))
	
	r, s, _ := ecdsa.Sign(rand.Reader, private, signHash)
	
  
	//end signing
  
	//convert the signature in to der structure
	signature := DerSignature{}
	signature.R = r
	signature.S = s
  
   //serialize the der structure signature
	derForm := signature.Serialize()
  
  
   //end serializing

  //hex encode signature
  dst := make([]byte, hex.EncodedLen(len(derForm)))
  hex.Encode(dst, derForm)
  
   //fmt.Printf("%s\n", dst)
  
   //joining all tx data
  
   finalTx := rawContext + ".ELAMA." + string(dst)
   return finalTx;

}

func mintTokens(num string) (tx string){
	_,priPEMData := readPemFromDisk("mintPrivateKey")

	private,_ := parse_pem_with_hex_private(priPEMData);
	
	
	
	//end creating key pairs
 



	
// 	//end der to address conversion
  
//    //creating transaction struct
	transaction := PrototypeTransaction{}
	transaction.Amount = num
	
//    //end creating transaction Struct
  
  
	//serialize transaction
	serializedTx,_ := transaction.Serialize()
	
	//end serializing transaction
  
	//encode serialize transaction in to base64
	rawContext := base64.StdEncoding.EncodeToString(serializedTx)
	//end encoding
  
	//create hash of encoded transaction
   
	 h := sha256.New()
	h.Write([]byte(rawContext))
	signHash := h.Sum(nil)

	
	r, s, _ := ecdsa.Sign(rand.Reader, private, signHash)
	
	//end signing
  
	//convert the signature in to der structure
	signature := DerSignature{}
	signature.R = r
	signature.S = s
  
   //serialize the der structure signature
	derForm := signature.Serialize()
  
  
   //end serializing

  //hex encode signature
  dst := make([]byte, hex.EncodedLen(len(derForm)))
  hex.Encode(dst, derForm)
  
   //fmt.Printf("%s\n", dst)
  
   //joining all tx data
  
   finalTx := rawContext + ".ELAMA." + string(dst)
  
   return finalTx;
  
}

func exchangeTx(amount string,receipient string) (tx string){
	
	formattedAddrOne := receipient

	_,priPEMData := readPemFromDisk("mintPrivateKey")
	private,_ := parse_pem_with_hex_private(priPEMData);

//    //creating exchange struct
	transaction := PrototypeExchange{}
	transaction.Amount = amount
	transaction.To = formattedAddrOne
	
//    //end creating transaction Struct
  
  
	//serialize transaction
	serializedTx,_ := transaction.Serialize()

	//end serializing transaction
  
	//encode serialize transaction in to base64
	rawContext := base64.StdEncoding.EncodeToString(serializedTx)
	//end encoding
  
	//create hash of encoded transaction
   
	 h := sha256.New()
	h.Write([]byte(rawContext))
	signHash := h.Sum(nil)
	
	r, s, _ := ecdsa.Sign(rand.Reader, private, signHash)


	//end signing
  
	//convert the signature in to der structure
	signature := DerSignature{}
	signature.R = r
	signature.S = s
  
   //serialize the der structure signature
	derForm := signature.Serialize()
  
  
   //end serializing

  //hex encode signature
  dst := make([]byte, hex.EncodedLen(len(derForm)))
  hex.Encode(dst, derForm)
  
   //fmt.Printf("%s\n", dst)
  
   //joining all tx data
  
   finalTx := rawContext + ".ELAMA." + string(dst)
   return finalTx;
}

func main(){
	fcn:= os.Args[1]

	if(fcn == "account") {
		accountTx:=CreateMintAccount();
		fmt.Println(accountTx);
		
	} else if(fcn == "mint") {
		amount:= os.Args[2]
		mintTx:=mintTokens(amount);
		fmt.Println(mintTx);
	} else if(fcn == "exchange"){
		amount:= os.Args[2]
		address:= os.Args[3]
		//dec432a6c9b9557a9a74338ac32179cde39fbfb1819b3df7d1ed2e32eeed39b0 
		exchangeTx:=exchangeTx(amount,address)
		fmt.Println(exchangeTx)
	} else {
 
		fmt.Println("Invalid args")
	}


	
}


